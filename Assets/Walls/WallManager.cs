﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallManager : MonoBehaviour {

    public GameObject wallObject;
    public int maxWalls = 10;
    public float timeToMove = 4.0f;
    public float minTimeToMove = 3.0f;
    public float decreaseTimer = 0.025f;
    public float initialVelocity = 0.5f;
    public float velocityIncrease = 0.05f;
    public float maxVelocity = 5.0f;
    public float maxDamage = 0.10f;
    public int maxDistanceDamage = 5;

    private PlayerController playerController;
    private List<WallBehaviour> wallsSpawned;
    private int mostRecentlyChanged = 0;
    private float wallHeight;
    private float wallLength;
    private int currentYPosition = 0;
    private float timer = 0.0f;

    WallBehaviour.WallData spawnData;

    // Use this for initialization
    void Start() {
        playerController = gameObject.GetComponent<PlayerController>();
        wallsSpawned = new List<WallBehaviour>();
        spawnData.wallVelocity = initialVelocity;

        wallHeight = wallObject.GetComponent<Renderer>().bounds.size.y;
        currentYPosition += (int)Mathf.Round(wallHeight*0.5f);
        wallLength = wallObject.GetComponent<Renderer>().bounds.size.x * 2;

        for (int i = 0; i < maxWalls; i++)
        {
            spawnData.timeToMove = timeToMove + (i * 1.0f);
            spawnData.spawnPosition = new Vector2Int(0, currentYPosition);
            wallsSpawned.Add(ScriptableObject.CreateInstance<WallBehaviour>());
            wallsSpawned[mostRecentlyChanged++].InitiateWalls(spawnData, ref wallObject);
            currentYPosition += (int)Mathf.Round(wallHeight);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(playerController.fear < 1.0f && findPlayerWallIndex() >= 0)
        {
            playerController.ApplyFearAttrition(calculateFearDamage());

            if (((currentYPosition - gameObject.transform.position.y) / wallHeight) < 5)
            {
                moveLowestWall();
            }
        }    
        else
        {
            playerController.Dead = true;
        }
    }

    // Update is called once per frame
    void Update () {
		foreach(WallBehaviour wallPair in wallsSpawned)
        {
            wallPair.selfUpdate();
        }
	}

    private int findPlayerWallIndex()
    {
        int floor = (int)(gameObject.transform.position.y / (int)Mathf.Round(wallHeight));



        return floor % maxWalls;
    }

    private float calculateFearDamage()
    {
        float fearDamage = 0.0f;
        int currentFloor = findPlayerWallIndex();
        float currentDistance = wallsSpawned[currentFloor].getWallGap();

        if (currentDistance > wallLength * maxDistanceDamage) fearDamage = maxDamage;
        else fearDamage = (currentDistance / (wallLength * maxDistanceDamage)) * maxDamage;

        return fearDamage;
    }

    private void moveLowestWall()
    {
        if (mostRecentlyChanged >= wallsSpawned.Count) mostRecentlyChanged = 0;
        if ((spawnData.wallVelocity += velocityIncrease) > maxVelocity) spawnData.wallVelocity = maxVelocity;
        if(timeToMove > minTimeToMove) timeToMove -= decreaseTimer;
        spawnData.timeToMove = timeToMove; //TODO: Playtest and tweak this
        spawnData.spawnPosition = new Vector2Int(0, currentYPosition);
        currentYPosition += (int)Mathf.Round(wallHeight);
        wallsSpawned[mostRecentlyChanged++].respawnWalls(spawnData);
    }
}
