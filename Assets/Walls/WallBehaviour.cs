﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallBehaviour : ScriptableObject {

    public struct WallData{
        public Vector2Int spawnPosition;
        public float timeToMove;
        public float wallVelocity;
    }

    public float timer = 0.0f;

    private WallData wallData;
    private GameObject leftWall;
    private GameObject rightWall;
    private float gameObjectLength;
    private bool wallsInitated = false;

    public void selfUpdate()
    {
        if (wallsInitated)
        {
            if((timer += Time.deltaTime) >= wallData.timeToMove)
                moveWalls();
        }
    }

    public void InitiateWalls(WallData wallValues, ref GameObject wallObject)
    {
        wallData = wallValues;
        timer = 0.0f;
        gameObjectLength = wallObject.GetComponent<Renderer>().bounds.size.x;
        leftWall = Instantiate(wallObject, new Vector3(wallData.spawnPosition.x - gameObjectLength, wallData.spawnPosition.y, 1.0f), Quaternion.identity);
        rightWall = Instantiate(wallObject, new Vector3(wallData.spawnPosition.x + gameObjectLength, wallData.spawnPosition.y, 1.0f), Quaternion.identity);
        wallsInitated = true;
    }

    public void moveWalls()
    {
        Vector3 leftPos = leftWall.transform.position;
        Vector3 rightPos = rightWall.transform.position;
        leftWall.transform.position = new Vector3(leftPos.x - wallData.wallVelocity * Time.deltaTime, leftPos.y, leftPos.z);
        rightWall.transform.position = new Vector3(rightPos.x + wallData.wallVelocity * Time.deltaTime, rightPos.y, rightPos.z);
    }

    public void respawnWalls(WallData wallValues)
    {
        wallData = wallValues;
        timer = 0.0f;
        leftWall.transform.position = new Vector3(wallData.spawnPosition.x - gameObjectLength, wallData.spawnPosition.y, 1.0f);
        rightWall.transform.position = new Vector3(wallData.spawnPosition.x + gameObjectLength, wallData.spawnPosition.y, 1.0f);
        wallsInitated = true;
    }

    public float getWallGap()
    {
        return rightWall.transform.position.x - leftWall.transform.position.x;
    }
}
