﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HudController : MonoBehaviour {

    public PlayerController player;
    public Text fearStatus;
    public Image InvertControllerImage;
    public Sprite normalControlsImage;
    public Sprite invertControlsImage;

    public Image MoveSpeedImage;
    public Sprite normalSpeedImage;
    public Sprite decreasedSpeedImage;

    public Image JumpForceImage;
    public Sprite normalJumpImage;
    public Sprite increasedJumpImage;

    public Slider fearBar;

    private bool buttonsSpawned = false;
    private float timerToPause;

    public void FixedUpdate()
    {
        //fearStatus.text = $"Fear: {(player.fear * 100).ToString("F0")}%";
        fearBar.value = player.fear;

        if (player.acceleration > 0) InvertControllerImage.sprite = normalControlsImage;
        else InvertControllerImage.sprite = invertControlsImage;

        if (player.acceleration == player.initialAcceleration) MoveSpeedImage.sprite = normalSpeedImage;
        else if (player.acceleration < player.initialAcceleration) MoveSpeedImage.sprite = decreasedSpeedImage;

        if (player.jumpForce == player.initialJumpForce) JumpForceImage.sprite = normalJumpImage;
        else if (player.jumpForce > player.initialJumpForce) JumpForceImage.sprite = increasedJumpImage;
    }
}
