﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLookat : MonoBehaviour {

    public PlayerController Player;
    public float CameraSpeed = 10f;
    public float playerMovementReduction = 0.2f;
    

    // Update is called once per frame
    void Update () {
        //float playerMovement = Player.CameraMovementData() * playerMovementReduction;
        if(Player.transform.position.y > transform.position.y)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, Player.transform.position.y, transform.position.z), 0.75f * Time.deltaTime);
        }
        transform.position += new Vector3(0, (CameraSpeed * Time.deltaTime), 0);
	}
}
