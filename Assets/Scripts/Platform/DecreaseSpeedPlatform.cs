﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecreaseSpeedPlatform : PlatformType
{
    [Range(3.0f, 6.0f)]
    [SerializeField] private float decreaseMaxSpeedAmount = 3.0f;

    [Range(200.0f, 450.0f)]
    [SerializeField] private float decreaseAccelerationAmount = 300.0f;

    public override void OnCollisionWithPlayer()
    {
        playerController.acceleration -= decreaseAccelerationAmount;
        playerController.maxSpeed -= decreaseMaxSpeedAmount;
    }

    public override void EndEffect()
    {
        playerController.acceleration += decreaseAccelerationAmount;
        playerController.maxSpeed += decreaseMaxSpeedAmount;
    }
}
