﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing; //VS cannot find this, but it's there, I promise!

public class InvertControllerPlatform : PlatformType
{
    public PostProcessVolume volume;

    UpsideDown ripples;

    public override void OnCollisionWithPlayer()
    {
        playerController.acceleration *= -1;
    }
    public override void SetupPostProcessingEffect( PostProcessVolume ppv )
    {
        volume = ppv;
        volume.profile.TryGetSettings(out ripples);
        ripples.enabled.value = true;
    }
    public override void EndEffect()
    {
        playerController.acceleration *= -1;
        volume.profile.TryGetSettings(out ripples);
        ripples.enabled.value = false;
    }
}
