﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: move head collider when flipX


public class CloneBehaviour : MonoBehaviour
{


    public float initialAcceleration = 900.0f;
    public float initialMaxSpeed = 12.0f;
    public float initialJumpForce = 800.0f;

    public Rigidbody2D rigidBody;
    public PlayerAnimator animator;

    public float acceleration = 900.0f;   // Arbitrary values, need some tweaking, the same with PlayerPhysics2D object
    public float maxSpeed = 12.0f;

    public float maxAdditionalSpeed = 10;
    public float maxAdditionalJump = 400;

    public LayerMask whatIsGround;
    private bool grounded;
    public BoxCollider2D feetCollider;

    private float horizontalMove = 0.0f;

    private float panicInputTimer;
    private float allowedSpace;

    private float yMovementSinceLastCall;
    // Use this for initialization
    void Start()
    {
        yMovementSinceLastCall = 0.0f;
        allowedSpace = 10.0f;
        panicInputTimer = 0.15f;

        animator = GetComponent<PlayerAnimator>();
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.freezeRotation = true;
        grounded = false;
    }

    private void FixedUpdate()
    {

        panicInputTimer += Time.fixedDeltaTime;
        if (panicInputTimer >= 0.15f)
        {
            panicInputTimer = 0;
            horizontalMove = Mathf.Sign(Random.Range(-1, 1));
        }


        float currentMaxSpeed = maxSpeed + maxAdditionalSpeed;

        if (horizontalMove * rigidBody.velocity.x < (maxSpeed + maxAdditionalSpeed))
            rigidBody.AddForce(Vector2.right * (horizontalMove * acceleration));

        if (Mathf.Abs(rigidBody.velocity.x) > (maxSpeed + maxAdditionalSpeed))
            rigidBody.velocity = new Vector2(Mathf.Sign(rigidBody.velocity.x) * maxSpeed, rigidBody.velocity.y);

        animator.UpdateVelocity(rigidBody.velocity);
        animator.UpdateFear(1);

        yMovementSinceLastCall = rigidBody.velocity.y;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.otherCollider == feetCollider)
        {
            grounded = true;
        }
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.otherCollider == feetCollider)
        {
            grounded = false;
        }
    }

}


