﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrampolinePlatform : PlatformType
{
    [Range(800.0f, 5000.0f)]
    [SerializeField] private float trampolineForce = 1600.0f;

    public AudioSource audioSource;
    public AudioClip trampolineSound;

    public override void OnCollisionWithPlayer()
    {
        playerController.rigidBody.velocity = new Vector2(playerController.rigidBody.velocity.x, 0.0f);
        playerController.rigidBody.AddForce(new Vector2(0.0f, trampolineForce));
        playerController.audioSource.PlayOneShot(trampolineSound);
    }

    public override void EndEffect()
    {
        // No effect to end
    }
}
