﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing; //VS cannot find this, but it's there, I promise!

public class PlatformType : MonoBehaviour {

    public int allowedFromHeight;
    public bool collidedWithPlayer = false;
    public bool effectEnded = false;
    [Tooltip("If effect is lasting for some time, set this, else let effectTimer be 0.0f")]
    public float effectTimer = 0.0f;
    private float effectLasted = 0.0f;

    public PlatformSpawner spawner;

    public GameObject player;
    public PlayerController playerController;
    public PostProcessController postProcessController;

    public SpriteRenderer spriteRenderer;

    private void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        player = playerController.gameObject;
        postProcessController = player.GetComponent<PostProcessController>();
        //playerController = player.GetComponent<PlayerController>();
    }

    public virtual void SetupPostProcessingEffect( PostProcessVolume ppp )
    {  }


    public void FixedUpdate()
    {
        if(effectTimer != 0.0f)
        {
            if (!effectEnded)
            {
                if (collidedWithPlayer)
                {
                    if ((effectLasted += Time.fixedDeltaTime) >= effectTimer)
                    {
                        EndEffect();
                        effectEnded = true;
                    }
                }
            }
        }
    }
    

    public void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (!collidedWithPlayer && playerController.rigidBody.velocity.y <= 0)
            {
                SetupPostProcessingEffect(postProcessController.postProcessVolume);
                effectEnded = false;
                effectLasted = 0;
                OnCollisionWithPlayer();
                collidedWithPlayer = true;
            }
        }
    }
    
    public virtual void OnCollisionWithPlayer() { }
    public virtual void EndEffect() { }

}
