﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing; //VS cannot find this, but it's there, I promise!

public class PanicClonePlatform : PlatformType
{
    public PostProcessVolume volume;

    FearRipples ripples;
    

    public GameObject clone;
    public AudioClip panicScream;
    List<GameObject> panicCloneList = new List<GameObject>();

    public int cloneCount = 5;

    public override void OnCollisionWithPlayer()
    {
        playerController.audioSource.PlayOneShot(panicScream, 0.5f);
        for (int i = 0; i < cloneCount; i++)
        {
            panicCloneList.Add(Instantiate(clone, playerController.rigidBody.transform.position, Quaternion.identity));
        }
       
    }

    public override void SetupPostProcessingEffect( PostProcessVolume ppv )
    {
        volume = ppv;
        volume.profile.TryGetSettings(out ripples);
        ripples.blend.value = 0.5f;
        ripples.enabled.value = true;
    }
    public override void EndEffect()
    {
        foreach(var clone in panicCloneList)
        {
            Destroy(clone);
        }

        volume.profile.TryGetSettings(out ripples);
        ripples.enabled.value = true;
    }
}