﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseJumpForcePlatform : PlatformType
{
    [Range(800.0f, 3200.0f)]
    [SerializeField] private float effectScale = 800.0f;

    public override void OnCollisionWithPlayer()
    {
        playerController.jumpForce += effectScale;
    }

    public override void EndEffect()
    {
        playerController.jumpForce -= effectScale;
    }
}
