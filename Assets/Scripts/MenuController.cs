﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    //Look into DontDestroyOnLoad on this, as we might reuse it a pause menu or something. 
    //i.e. throughout the game meaning we'll need to carry the reference over between scenes.

    public void StartGame()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);//might trigger the animation, then load async instead... let's wait till we have an animation though...
    }
    
    public void LoadScene(int i)
    {
        SceneManager.LoadScene(i, LoadSceneMode.Single);
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
