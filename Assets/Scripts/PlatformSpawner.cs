﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlatformSpawner : MonoBehaviour
{
    public PlayerController playerController;

    public PlatformType[] platformPrototypes;
    Dictionary<Type, Queue<GameObject>> platformPools;

    Type[] platformTypes;


    public int poolSize = 4;
    public float spawnWaitTime = 1;

    public bool isSpawning;
    private int spawnHeight = 0;
    Queue<PlatformType> activePlatforms = new Queue<PlatformType>();

    [MinMaxSlider(4,20)]
    public Vector2Int minMaxPlatformSize;

    int lastPlatformWidth = 4; //initial offsetting when set

    private void Start()
    {
        FillPools(poolSize);
    }

    private void FillPools(int poolSize)
    {
        platformPools = new Dictionary<Type, Queue<GameObject>>(platformPrototypes.Length);
        platformTypes = new Type[platformPrototypes.Length];
        for (int i = 0; i < platformPrototypes.Length; i++)
        {
            Type platformType = platformPrototypes[i].GetType();
            platformTypes[i] = platformType;
            platformPools[platformType] = new Queue<GameObject>(poolSize);
            for (int j = 0; j < poolSize; j++)
            {
                platformPrototypes[i].spawner = this;
                GameObject go = Instantiate(platformPrototypes[i].gameObject);
                go.SetActive(false);
                platformPools[platformType].Enqueue(go);
            }
        }
    }


    private void OnTriggerEnter2D( Collider2D collision )
    {
        if (collision.gameObject.CompareTag("Player"))
            StartCoroutine(SpawnPlatform());
    }

    IEnumerator SpawnPlatform()
    {
        spawnHeight += 3;
        Vector2 spawnOffset;
        if (playerController.jumpForce < playerController.initialJumpForce)
        {
            spawnOffset = Vector2.up * spawnHeight * playerController.jumpForce / playerController.initialJumpForce;
        }
        else
        {
            spawnOffset = Vector2.up * spawnHeight;
        }

        transform.position = spawnOffset; //move the trigger along

        int direction = (int)Mathf.Sign(Random.Range(-1,1));

        if (playerController.maxSpeed < playerController.initialMaxSpeed)
        {
            float justify = (playerController.maxSpeed / playerController.initialMaxSpeed);

            spawnOffset += new Vector2(direction * (lastPlatformWidth * 0.5f * Random.Range(-justify, justify) + 3), 0);
        }
        else
        {
            spawnOffset += new Vector2( direction * (lastPlatformWidth * 0.5f + 3), 0);
        }

        yield return new WaitForSeconds(spawnWaitTime);

        if (activePlatforms.Count > poolSize)
        {
            PlatformType expendable = activePlatforms.Dequeue();
            //expendable.gameObject.SetActive(false); //AllowThis to be removed
            platformPools[expendable.GetType()].Enqueue(expendable.gameObject);
        }

        int platformTypeIndex = Random.Range(0, platformTypes.Length);
        GameObject go = platformPools[platformTypes[platformTypeIndex]].Dequeue();
        PlatformType platform = go.GetComponent<PlatformType>();
        EdgeCollider2D collider = platform.GetComponent<EdgeCollider2D>();

        lastPlatformWidth = Random.Range(minMaxPlatformSize.x, minMaxPlatformSize.y);
        platform.spriteRenderer.size = new Vector2(lastPlatformWidth, 1.5f);
        collider.points = new Vector2[2] { Vector2.left * (lastPlatformWidth * 0.5f - 0.1875f), Vector2.right * (lastPlatformWidth * 0.5f - 0.1875f) };

        go.transform.position = spawnOffset; //TODO: Calculate next platform position

        go.SetActive(true);
        activePlatforms.Enqueue(platform);
    }

}
