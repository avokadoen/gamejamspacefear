﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.PostProcessing;

//NOTE: Visual Studio Complains about not finding the different references... That's fine, Unity will do!
// It's nice when people tell you about this kind of thing, so you don't need to spend >hour finding this out yourself, by coincidence. 
// AFTER TRYING FREAKING EVERYTHING TO FIX IT
// THANKS A LOT GOOGLE 😠😠😠😠😠

//Sidenote: it might be possible to actually set up the references to the post processing stack-dlls manually in vs, tho I won't bother for now. THEY WORK! :D

[Serializable]
[PostProcess(typeof(UpsideDownRenderer), PostProcessEvent.AfterStack, "Custom/UpsideDown")]
public sealed class UpsideDown : PostProcessEffectSettings{}

public sealed class UpsideDownRenderer : PostProcessEffectRenderer<UpsideDown>
{
    public override void Render( PostProcessRenderContext context )
    {
        var sheet = context.propertySheets.Get(Shader.Find("Hidden/Custom/UpsideDown"));
        //TODO add all properties
        context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
    }
}