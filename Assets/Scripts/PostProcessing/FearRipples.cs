﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.PostProcessing;

//NOTE: Visual Studio Complains about not finding the different references... That's fine, Unity will do!
// It's nice when people tell you about this kind of thing, so you don't need to spend >hour finding this out yourself, by coincidence. 
// AFTER TRYING FREAKING EVERYTHING TO FIX IT
// THANKS A LOT GOOGLE 😠😠😠😠😠

//Sidenote: it might be possible to actually set up the references to the post processing stack-dlls manually in vs, tho I won't bother for now. THEY WORK! :D

[Serializable]
[PostProcess(typeof(FearRipplesRenderer), PostProcessEvent.AfterStack, "Custom/FearRipples")]
public sealed class FearRipples : PostProcessEffectSettings
{
    [Range(0f, 1f), Tooltip("Ripple intensity.")]
    public FloatParameter blend = new FloatParameter { value = 0.5f };
}

public sealed class FearRipplesRenderer : PostProcessEffectRenderer<FearRipples>
{
    public override void Render( PostProcessRenderContext context )
    {
        var sheet = context.propertySheets.Get(Shader.Find("Hidden/Custom/FearRipples"));
        sheet.properties.SetFloat("_Blend", settings.blend);
        //TODO add all properties
        context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
    }
}