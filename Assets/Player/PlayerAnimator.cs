﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour {

    public SpriteRenderer spriteRenderer;
    public Animator animator;

    private bool isIdle;
    private bool isGrounded;

	// Use this for initialization
	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();

        isIdle      = true;
        isGrounded  = true;
        animator.SetBool("isIdle", isIdle);
    }
	
    public void UpdateVelocity(Vector2 velocity)
    {
        animator.SetFloat("yVelocity", velocity.y);
        animator.SetFloat("xVelocity", System.Math.Abs(velocity.x));
        if(velocity.x > 0.05f)
        {
            spriteRenderer.flipX = true;
        }
        else if(velocity.x < -0.05f)
        {
            spriteRenderer.flipX = false;
        }
    }

    public void UpdateFear(float fear)
    {
        animator.SetFloat("fear", fear);
    }


}
