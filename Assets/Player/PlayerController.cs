﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: move head collider when flipX


public class PlayerController : MonoBehaviour {

    
    public float initialAcceleration = 900.0f;
    public float initialMaxSpeed = 12.0f;
    public float initialJumpForce = 800.0f;


    public Rigidbody2D rigidBody;
    public PlayerAnimator animator;

    private new ParticleSystem.MainModule particleSystem;
    public int maxParticles = 200;

    [HideInInspector]public float acceleration   = 900.0f;   // Arbitrary values, need some tweaking, the same with PlayerPhysics2D object
    [HideInInspector]public float maxSpeed       = 12.0f;
    [HideInInspector]public float jumpForce      = 800.0f;
    public float keyPressTimer  = 1.2f;

    public float maxAdditionalSpeed = 10;
    public float maxAdditionalJump = 400;
    
    public bool Dead { get; set; }
    public float score;
    public float scoreMultiplier = 10.0f;

    public Transform groundCheck;
    public float groundRadius = 1.0f;
    public LayerMask whatIsGround;
    private bool grounded;
    public BoxCollider2D feetCollider;

    private float horizontalMove = 0.0f;
    private bool jump = false;

    // -- Input variables to "queue" input
    private bool pressJump = false;
    private float pressJumpTimer = 0.0f;
    private float jumpCooldown = 0.0f;

    public float fear;
    private float panicInputTimer;
    private float allowedSpace;

    private float yMovementSinceLastCall;

    public AudioSource audioSource;
    public AudioClip screamingMan;
    public AudioClip dyingMan;
    public AudioClip jumpSound;

    private bool deadAudioPlayed;
    private bool screamAudioPlayed;
    // Use this for initialization
    void Start () {

        acceleration = initialAcceleration;
        maxSpeed = initialMaxSpeed;
        jumpForce = initialJumpForce;


        Dead = false;

        deadAudioPlayed = false;
        screamAudioPlayed = false;
        fear = 0.0f;
        score = 0.0f;
        yMovementSinceLastCall = 0.0f;
        allowedSpace = 10.0f;
        panicInputTimer = 0.15f;

        animator        = GetComponent<PlayerAnimator>();
        rigidBody       = GetComponent<Rigidbody2D>();
        particleSystem  = GetComponent<ParticleSystem>().main;
        audioSource     = GetComponent<AudioSource>();
        rigidBody.freezeRotation = true;
        grounded = false;
    }

    private void FixedUpdate()
    {
        if (!Dead) score += Time.fixedDeltaTime * scoreMultiplier;
        else if (Dead && !deadAudioPlayed)
        {
            audioSource.PlayOneShot(dyingMan);
            deadAudioPlayed = true;
        }
            
        if (fear < 1)
        {
            horizontalMove = Input.GetAxis("Horizontal");
        }
        else
        {
            if (!screamAudioPlayed)
            {
                audioSource.PlayOneShot(screamingMan, 0.7f);
                screamAudioPlayed = true;
            }
            panicInputTimer += Time.deltaTime;
            if (panicInputTimer >= 0.15f)
            {
                panicInputTimer = 0;
                horizontalMove = Mathf.Sign(Random.Range(-1, 1));
            }
            
        }

        float currentMaxSpeed = maxSpeed + maxAdditionalSpeed * fear;
        float currentJumpForce = jumpForce + maxAdditionalJump * fear;

        if (horizontalMove * rigidBody.velocity.x < (maxSpeed + maxAdditionalSpeed))
            rigidBody.AddForce(Vector2.right * (horizontalMove * acceleration));

        if (Mathf.Abs(rigidBody.velocity.x) > (maxSpeed + maxAdditionalSpeed))
            rigidBody.velocity = new Vector2(Mathf.Sign(rigidBody.velocity.x) * maxSpeed, rigidBody.velocity.y);

        if (jump)
        {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, 0);
            rigidBody.AddForce(new Vector2(0.0f, currentJumpForce));
            audioSource.PlayOneShot(jumpSound, 0.4f);
            jump = false;
        }
        animator.UpdateVelocity(rigidBody.velocity);
        animator.UpdateFear(fear);
        particleSystem.maxParticles = (int)Mathf.Round(maxParticles * fear);

        yMovementSinceLastCall = rigidBody.velocity.y;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.otherCollider == feetCollider)
        {
            grounded = true;
        }
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.otherCollider == feetCollider)
        {
            grounded = false;
        }
    }

    void Update () {
        if(fear < 1)
        {
            if (grounded && pressJump)
            {
                jump = true;
                pressJump = false;
                pressJumpTimer = 0.0f;
            }
            else
            {
                if (grounded) pressJump = Input.GetButtonDown("Jump");
                else
                {
                    if (pressJump && ((pressJumpTimer += Time.deltaTime) < keyPressTimer))
                    {
                        pressJump = true;
                    }
                    else if (pressJump && ((pressJumpTimer) > keyPressTimer))
                    {
                        pressJumpTimer = 0.0f;
                        pressJump = false;
                    }
                    else
                    {
                        pressJump = Input.GetKeyDown(KeyCode.W);
                    }
                }
            }
        }
    }

    public void ApplyFearAttrition(float fearAttrition)
    {
        if (fear >= 1) { fear = 1; }
        else
        {
            fear -= 0.05f * Time.fixedDeltaTime;
            fear += fearAttrition * Time.fixedDeltaTime;
            if (fear < 0) { fear = 0; }
        }
        
    }

    public float CameraMovementData()
    {
        float rtrValue = yMovementSinceLastCall;
        yMovementSinceLastCall = 0;
        return rtrValue;
    }

}


