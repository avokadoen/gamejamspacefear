﻿Shader "Hidden/Custom/FearRipples"
{
    HLSLINCLUDE

        #include "PostProcessing/Shaders/StdLib.hlsl"
        #include "noiseSimplex.cginc"

        TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
        float _Blend;

        float4 Frag(VaryingsDefault i) : SV_Target
        {
             float2 pix = float2(4.0 / _ScreenParams.x, 4.0 / _ScreenParams.y);

             float2 pixUV = float2(
                pix.x * round(i.texcoord.x / pix.x),
                pix.y * round(i.texcoord.y / pix.y)
                );


            float4 color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, pixUV + _Blend.x*snoise(float3(pixUV, _Time.y)).xx*0.0625);
            return color;
        }

    ENDHLSL

    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            HLSLPROGRAM

            #pragma vertex VertDefault
            #pragma fragment Frag

            ENDHLSL
        }
    }
}