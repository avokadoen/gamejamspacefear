﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DeathTrigger : MonoBehaviour {

    public UnityEvent onDeathTrigger;

    private void OnTriggerEnter2D( Collider2D collision )
    {
        if(collision.gameObject.CompareTag("Player"))
            onDeathTrigger.Invoke();
    }
}
